package com.example.iqviaassignment.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.iqviaassignment.converter.MedicationConverter;
import com.example.iqviaassignment.medicationEntity.MedicationEntity;
import com.example.iqviaassignment.medicationRepo.MedicationRepo;

class MedicationServiceImplTest {
	
	    @InjectMocks
	    MedicationServiceImpl service;
	    
	    @Mock
		MedicationConverter medicationConverter;
	    
	    @Mock
		MedicationRepo medicationRepo;


	@Test
	void testAddMedication() {
		MedicationEntity entity=new MedicationEntity();
		when(medicationRepo.save(entity)).thenReturn(entity);
		 assertNotNull(service.addMedication(entity));
	}

}
