package com.example.iqviaassignment.service;


import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import com.example.iqviaassignment.converter.MedicationConverter;
import com.example.iqviaassignment.converter.PatientConverter;
import com.example.iqviaassignment.dto.MedicationDto;
import com.example.iqviaassignment.dto.PatientDto;
import com.example.iqviaassignment.medicationEntity.MedicationEntity;
import com.example.iqviaassignment.medicationRepo.MedicationRepo;
import com.example.iqviaassignment.patientEntity.PatientDataEntity;
import com.example.iqviaassignment.patientRepo.PatientRepo;
import com.example.iqviaassignment.utils.CustomException;


@ExtendWith(MockitoExtension.class)
class PatientDataServiceImplTest {
		
	@InjectMocks
	PatientDataServiceImpl service;
	
	@Mock
	PatientRepo patientRepository;
	@Mock
	MedicationRepo medicationRepo;
	
	@Mock
	PatientConverter patientConverter;
	@Mock
	MedicationConverter medicationConverter;
	
	@Test
	void testFindByEmailId() {
		PatientDataEntity entity=new PatientDataEntity();
		PatientDto patientDto= new PatientDto();
		List<MedicationEntity> medicationEntityList = new ArrayList<>();
		List<MedicationDto> medicationDtoEntityLis = new ArrayList<>();
		
		entity.setFirstName("Jayshree");
		entity.setAge(25);
		entity.setLastName("More");
		entity.setEmailId("jayu@gmail.com");
		entity.setPatientId(1L);
		
		when(patientRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.ofNullable(entity));
	    when(patientConverter.toDto(entity)).thenReturn(patientDto);
		when(medicationRepo.findBypatientId(Mockito.anyLong())).thenReturn(medicationEntityList);
		when(medicationConverter.toDtoList(medicationEntityList)).thenReturn(medicationDtoEntityLis);
	//	when(patientDto.setMedication(medicationDtoEntityLis));
		
		 assertNotNull(service.findByEmailId(Mockito.anyString()));
	}

	
	@Test
	void testFindByEmailId_CustomException() throws CustomException {
		PatientDataEntity entity=null;
		
		//String emailId="";
		when(patientRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
		assertThrows(CustomException.class, ()-> service.findByEmailId(Mockito.anyString()));
		
		//.thenThrow(new CustomException(HttpStatus.NOT_FOUND,"Email ID Doesn't exist Please Enter valid emailId"));

		//assertEqual(service.findByEmailId(""));
	}
//	
	@Test
	void testSavePatient() 
	{
		PatientDataEntity patientEntity=new PatientDataEntity();
		PatientDto patientDto= new PatientDto();
		List<MedicationEntity> medicationEntityList = new ArrayList<>();
		List<MedicationDto> medicationDtoList = patientDto.getMedication();
		patientEntity.setFirstName("Jayshree");
		patientEntity.setAge(25);
		patientEntity.setLastName("More");
		patientEntity.setEmailId("jayu@gmail.com");
		patientEntity.setPatientId(1L);
		System.out.println(Optional.ofNullable(patientEntity));
		when(patientRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.ofNullable(patientEntity));
//		 when(patientConverter.toEntity(patientDto)).thenReturn(patientEntity);
//		 when(patientRepository.save(patientEntity)).thenReturn(patientEntity);
//		 when(medicationConverter.toEntity(medicationDtoList)).thenReturn(medicationEntityList);
//		 when(medicationRepo.saveAll(medicationEntityList)).thenReturn(medicationEntityList);
		assertNotNull(service.savePatient(patientDto));
}
	@Test
	void testUpdatePatient() 
	{
		PatientDataEntity patientEntity=new PatientDataEntity();
		PatientDto patientDto= new PatientDto();
		List<MedicationEntity> medicationEntityList = new ArrayList<>();
		List<MedicationDto> medicationDtoList = patientDto.getMedication();
		patientEntity.setFirstName("Jayshree");
		patientEntity.setAge(25);
		patientEntity.setLastName("More");
		patientEntity.setEmailId("jayu@gmail.com");
		patientEntity.setPatientId(1L);	
		 when(patientConverter.toEntity(patientDto)).thenReturn(patientEntity);
		 when(patientRepository.save(patientEntity)).thenReturn(patientEntity);
		 when(medicationConverter.toEntity(medicationDtoList)).thenReturn(medicationEntityList);
		 when(medicationRepo.saveAll(medicationEntityList)).thenReturn(medicationEntityList);
		 assertNotNull(service.updatePatient(patientDto));
}
}