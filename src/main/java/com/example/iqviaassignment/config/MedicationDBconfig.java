package com.example.iqviaassignment.config;


import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.example.iqviaassignment.medicationRepo.MedicationRepo;




@Configuration
@EnableTransactionManagement
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@EnableJpaRepositories(basePackageClasses=MedicationRepo.class,entityManagerFactoryRef ="medicationDSEmFactory", transactionManagerRef = "medicationDSTransactionManager")
public class MedicationDBconfig extends BaseConfig{
	
	@Value("${spring.jpa.database-platform}")
private String dbPlatform;
@Value("${spring.jpa.show-sql}")
private String showSql;
@Value("${spring.jpa.hibernate.ddl-auto}")
private String hbm2DDLAuto;
	


@Bean
public JpaProperties medicationJpaProperties() {
  return getJpaProperties();
}
	@Bean()
	@ConfigurationProperties("spring.datasource.secondary")
	public DataSourceProperties medicationProperties() {
		return new DataSourceProperties();
	}
		
	@Bean
	public LocalContainerEntityManagerFactoryBean medicationDSEmFactory(final JpaProperties medicationJpaProperties
			) {
		 EntityManagerFactoryBuilder builder = createEntityManagerFactory(medicationJpaProperties);

		return builder.dataSource(medicationDataSource()).packages("com.example.iqviaassignment.medicationEntity").build();
		
	}
	 @Primary
	 @Bean
	  @ConfigurationProperties(prefix = "datasource.secondary.configurations")
	  public DataSource medicationDataSource() {
	    DataSource dataSource = medicationProperties()
	        .initializeDataSourceBuilder()
	        .type(BasicDataSource.class).build();
	    return dataSource;
	 }
	
	@Bean
	public PlatformTransactionManager medicationDSTransactionManager(final JpaProperties medicationDSEmFactory) {
		return new JpaTransactionManager(medicationDSEmFactory(medicationDSEmFactory).getObject());
		
	}

}
