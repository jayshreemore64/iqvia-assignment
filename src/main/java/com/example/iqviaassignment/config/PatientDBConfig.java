package com.example.iqviaassignment.config;



import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.example.iqviaassignment.patientRepo.PatientRepo;



@Configuration
@EnableTransactionManagement
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@EnableJpaRepositories(basePackageClasses = PatientRepo.class, entityManagerFactoryRef ="patientDbFactory", transactionManagerRef = "patientmanager")
public class PatientDBConfig extends BaseConfig {
	
@Value("${spring.jpa.database-platform}")
private String dbPlatform;
@Value("${spring.jpa.show-sql}")
private String showSql;
@Value("${spring.jpa.hibernate.ddl-auto}")
private String hbm2DDLAuto;
	

@Primary
@Bean
public JpaProperties patientJpaProperties() {
  return getJpaProperties();
}

    
	@Bean()
	@ConfigurationProperties("spring.datasource.primary")
	public DataSourceProperties patientProperties() {
		return new DataSourceProperties();
	}

	@Primary
	@Bean
	public LocalContainerEntityManagerFactoryBean patientDbFactory(	final JpaProperties patientJpaProperties) 
	{
		 EntityManagerFactoryBuilder builder = createEntityManagerFactory(patientJpaProperties);
		 
//		HashMap<String, Object> properties = new HashMap<>();
//		properties.put("hibernate.hbm2ddl.auto", "update");
//		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		
		return builder.dataSource(patientDataSource()).packages("com.example.iqviaassignment.patientEntity").build();
		
	}
	 
	 @Bean
	  @ConfigurationProperties(prefix = "datasource.primary.configurations")
	  public DataSource patientDataSource() {
	    DataSource dataSource = patientProperties()
	        .initializeDataSourceBuilder()
	        .type(BasicDataSource.class).build();
	    return dataSource;
	 }

	 @Primary
	 @Bean
	  public JpaTransactionManager patientmanager(final JpaProperties patientJpaProperties) {
	    return new JpaTransactionManager(patientDbFactory(patientJpaProperties).getObject());
	  }
}
