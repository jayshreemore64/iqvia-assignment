package com.example.iqviaassignment.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.example.iqviaassignment.dto.MedicationDto;
import com.example.iqviaassignment.medicationEntity.MedicationEntity;

@Component
public class MedicationConverter implements BaseConverter<MedicationEntity,MedicationDto> {

	@Override
	public MedicationEntity toEntity(MedicationDto dto) {
		MedicationEntity entity = new MedicationEntity();
		BeanUtils.copyProperties(dto, entity);
		return entity;
	}

	@Override
	public MedicationDto toDto(MedicationEntity entity) {
		MedicationDto dto = new MedicationDto();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}
	
	public List<MedicationEntity> toEntityList(List<MedicationDto> list) {
		return list.stream().map(this::toEntity).collect(Collectors.toList());
	}
	
	public List<MedicationDto> toDtoList(List<MedicationEntity> medicationEntity) {
		return medicationEntity.stream().map(this::toDto).collect(Collectors.toList());
	}

}
