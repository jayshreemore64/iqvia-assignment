package com.example.iqviaassignment.converter;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.example.iqviaassignment.dto.PatientDto;
import com.example.iqviaassignment.patientEntity.PatientDataEntity;
@Component
public class PatientConverter implements BaseConverter<PatientDataEntity,PatientDto> {
	
	public PatientDataEntity toEntity(PatientDto dto) {
		PatientDataEntity entity = new PatientDataEntity();		
		BeanUtils.copyProperties(dto, entity);
		return entity;
	}

	

	@Override
	public PatientDto toDto(PatientDataEntity entity) {
		PatientDto dto = new PatientDto();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

}
