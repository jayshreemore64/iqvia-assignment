package com.example.iqviaassignment.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class PatientDto {

	
	private Long patientId;
	private String firstName;
	private String lastName;
	private String emailId;
	private int age;
	private List<MedicationDto> medication;
}
