package com.example.iqviaassignment.dto;





import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class MedicationDto {

private Long patientMedicationId;
	
	
	private String appointmentId;
	
	private String drugManufacturerName;

	private String medicationDetails;

	private Long patientId;

	
}
