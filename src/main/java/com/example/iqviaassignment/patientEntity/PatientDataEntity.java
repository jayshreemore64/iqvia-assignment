package com.example.iqviaassignment.patientEntity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;





@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "patient_Data",schema="patientschema")
//schema="patientschema"
public class PatientDataEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "patient_id")
	private Long patientId;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "email_id")
	private String emailId;
	
	@Column(name = "age")
	private int age;

//	@OneToMany(targetEntity=MedicationEntity.class, cascade = CascadeType.ALL)
//	 @JoinColumn(name="patient_id")
//	private List<MedicationEntity> patient_medication;
	
   //	@OneToMany(targetEntity=MedicationEntity.class, cascade = CascadeType.ALL)
	// @JoinTable(name = "medication", joinColumns = {@JoinColumn(name="patientMedicationId")})
//             inverseJoinColumns = {@JoinColumn(name="scope_topic_id")} )
	//private List<MedicationEntity> patientmedication;
//	 @OneToMany(targetEntity=MedicationEntity.class, cascade = CascadeType.ALL)
//	 @JoinColumn(name="patient_id")
//	 private List<MedicationEntity> patientmedication;
	
//	@OneToMany(targetEntity=City.class, cascade = CascadeType.ALL)
//	@JoinColumn(name="patient_id")
//	private List<City> city;

}
