package com.example.iqviaassignment.medicationRepo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.iqviaassignment.medicationEntity.MedicationEntity;

@Repository
public interface MedicationRepo extends JpaRepository<MedicationEntity,Long> {
	List<MedicationEntity> findBypatientId(Long patientId);
}
