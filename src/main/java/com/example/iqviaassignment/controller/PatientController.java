package com.example.iqviaassignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.iqviaassignment.constant.UrlConstant;
import com.example.iqviaassignment.dto.PatientDto;
import com.example.iqviaassignment.service.PatientDataService;
import com.example.iqviaassignment.utils.ApiResponse;
import com.example.iqviaassignment.utils.CustomException;
import com.example.iqviaassignment.utils.ResponseUtil;

@RestController
@RequestMapping("")
public class PatientController {

	@Autowired
	PatientDataService patientDataService;
	
	@GetMapping(UrlConstant.URL_PatientByEmail)
	public ResponseEntity<ApiResponse> getPatientByemail(@RequestParam String emailId){
	
		 return ResponseUtil.getResponse(HttpStatus.OK, "Data fetch Successful",this.patientDataService.findByEmailId(emailId));
	}
	
//	@GetMapping(UrlConstant.URL_PatientByName)
//	public ResponseEntity<ApiResponse> getPatientByName(@RequestParam String name) throws CustomException{
//	
//		 return ResponseUtil.getResponse(HttpStatus.OK, "Data fetch Successful",this.patientDataService.findByFirstName(name));
//	}
	@PostMapping(UrlConstant.URL_Patient)
	public ResponseEntity<ApiResponse> savePatient(@RequestBody PatientDto patient){
	
		 return ResponseUtil.getResponse(HttpStatus.OK, "Data Save Successful",this.patientDataService.savePatient(patient));
	}
	
	@PutMapping(UrlConstant.URL_Patient)
	public ResponseEntity<ApiResponse> updatePatient( @RequestBody PatientDto patient) throws CustomException{
		this.patientDataService.updatePatient(patient);
		
		 return ResponseUtil.getResponse(HttpStatus.OK, "Data Update Successful");
	}
}
