package com.example.iqviaassignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iqviaassignment.constant.UrlConstant;
import com.example.iqviaassignment.medicationEntity.MedicationEntity;
import com.example.iqviaassignment.service.MedicationService;
import com.example.iqviaassignment.utils.ApiResponse;
import com.example.iqviaassignment.utils.ResponseUtil;

@RestController
@RequestMapping("")
public class MedicationController {
	
	@Autowired
	MedicationService medicationService;
	
	@PostMapping(UrlConstant.URL_medication)
	public ResponseEntity<ApiResponse> savePatient(@RequestBody MedicationEntity medication){
	
		 return ResponseUtil.getResponse(HttpStatus.OK, "Data Save Successful",this.medicationService.addMedication(medication));
	}

}
