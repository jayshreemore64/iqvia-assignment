package com.example.iqviaassignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.iqviaassignment.converter.MedicationConverter;
import com.example.iqviaassignment.medicationEntity.MedicationEntity;
import com.example.iqviaassignment.medicationRepo.MedicationRepo;
import com.example.iqviaassignment.utils.CustomException;
@Service
public class MedicationServiceImpl implements MedicationService {
	
	    @Autowired
		MedicationConverter medicationConverter;
	    
	    @Autowired
		MedicationRepo medicationRepo;

	@Override
	public MedicationEntity addMedication(MedicationEntity entity) {
		MedicationEntity result= medicationRepo.save(entity);
		 return result;
			}


}
