package com.example.iqviaassignment.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.iqviaassignment.converter.MedicationConverter;
import com.example.iqviaassignment.converter.PatientConverter;
import com.example.iqviaassignment.dto.PatientDto;
import com.example.iqviaassignment.medicationEntity.MedicationEntity;
import com.example.iqviaassignment.medicationRepo.MedicationRepo;
import com.example.iqviaassignment.patientEntity.PatientDataEntity;
import com.example.iqviaassignment.patientRepo.PatientRepo;
import com.example.iqviaassignment.utils.CustomException;

@Service
public class PatientDataServiceImpl implements PatientDataService {
	
	Logger logger = LoggerFactory.getLogger(PatientDataServiceImpl.class);
	
	@Autowired
	private PatientRepo patientRepository;
	
	@Autowired
	MedicationRepo medicationRepo;

	@Autowired
	PatientConverter patientConveter;
	
	 @Autowired(required=true)
	MedicationConverter medicationConverter;
	
	@Override
	public PatientDto findByEmailId(String emailId)  {
		Optional<PatientDataEntity> patient=patientRepository.findByEmailId(emailId);
		PatientDto patientdto=null;
		
		if(patient.isPresent()&&patient.get().getEmailId()!=null)
		{
			 PatientDataEntity patientData=patient.get();
				patientdto=  patientConveter.toDto(patientData);
				logger.info("Patient Data => {}",patientData);
				 List<MedicationEntity> medicationEntity= medicationRepo.findBypatientId(patient.get().getPatientId());
				  patientdto.setMedication(  medicationConverter.toDtoList(medicationEntity));		
		}
		
	
		else {
			logger.error("Invalid email Id => {} ." ,emailId);
			 throw new CustomException(HttpStatus.NOT_FOUND,"Email ID Doesn't exist Please Enter valid emailId");
	}
		return patientdto;
	}

	@Override
	public PatientDataEntity savePatient(PatientDto patient) {
		Optional<PatientDataEntity> patientDataPresent=patientRepository.findByEmailId(patient.getEmailId());
	    PatientDataEntity result;
		if(patientDataPresent.isPresent()) {
			logger.error("This emaild Id Already Used. => {} ." ,patient);
			 throw new CustomException(HttpStatus.NOT_FOUND,"Email ID Already exists");
		}
		
		     else {
		    	 PatientDataEntity patientEntity=  patientConveter.toEntity(patient);
			     result= patientRepository.save(patientEntity);
				     if(result!=null)
				     {
				    	 List<MedicationEntity>  medicationEntity=medicationConverter.toEntity(patient.getMedication());
				    	medicationEntity.forEach(x->x.setPatientId(result.getPatientId()));
				    	 List<MedicationEntity> entity=medicationRepo.saveAll(medicationEntity);
				    	System.out.println(medicationRepo.saveAll(entity));
				    	 
				     }
			}
		     
		return result;
	}

	@Override
	public PatientDto updatePatient(PatientDto patientData) {
		
		 PatientDataEntity patientEntity=  patientConveter.toEntity(patientData);
		 patientEntity.setPatientId(patientData.getPatientId());
		 PatientDataEntity result= patientRepository.save(patientEntity);
	    
	     if(result!=null)
	     {
	    	 List<MedicationEntity>  medicationEntity=medicationConverter.toEntity(patientData.getMedication());
	    	medicationEntity.forEach(x->x.setPatientId(result.getPatientId()));
	    	medicationRepo.saveAll(medicationEntity);
	    	 
	     }
		return patientData;		
	}
	
}
