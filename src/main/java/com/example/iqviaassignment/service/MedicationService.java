package com.example.iqviaassignment.service;

import com.example.iqviaassignment.dto.PatientDto;
import com.example.iqviaassignment.medicationEntity.MedicationEntity;
import com.example.iqviaassignment.patientEntity.PatientDataEntity;
import com.example.iqviaassignment.utils.CustomException;

public interface MedicationService {

	 public MedicationEntity addMedication(MedicationEntity entity );
		
	
}
