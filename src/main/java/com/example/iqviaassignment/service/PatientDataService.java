package com.example.iqviaassignment.service;

import com.example.iqviaassignment.dto.PatientDto;
import com.example.iqviaassignment.patientEntity.PatientDataEntity;

public interface PatientDataService {

 public PatientDto findByEmailId(String emailId );	
 public PatientDataEntity savePatient(PatientDto patient);
 public PatientDto updatePatient(PatientDto patientData);
}
