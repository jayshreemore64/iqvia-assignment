package com.example.iqviaassignment.patientRepo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.iqviaassignment.patientEntity.PatientDataEntity;

@Repository
public interface PatientRepo extends JpaRepository<PatientDataEntity,Long> {
	
//	@Query(value="select * from patientschema.patient_data as p inner join medicationschema.medication_data as m \r\n" + 
//			"on p.patient_id=m.patient_id", nativeQuery = true)
	Optional<PatientDataEntity> findByEmailId(String emailId);
	Optional<PatientDataEntity> findByFirstName(String firstName);
	
	
}
