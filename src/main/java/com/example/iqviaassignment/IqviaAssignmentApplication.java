package com.example.iqviaassignment;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.XADataSourceAutoConfiguration;



@SpringBootApplication()
//(exclude = {DataSourceAutoConfiguration.class, XADataSourceAutoConfiguration.class})
public class IqviaAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(IqviaAssignmentApplication.class, args);
	}
	
	
}
