package com.example.iqviaassignment.medicationEntity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "medication_data", schema="medicationschema" )
//schema="medicationschema"
public class MedicationEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "patient_medication_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long patientMedicationId;
	
	@Column(name = "appointment_id")
	private String appointmentId;
	
	

	@Column(name = "drugManufacturer_name")
	private String drugManufacturerName;
	
	@Column(name = "medication_details")
	private String medicationDetails;
	
	@Column(name = "patient_id")
	private Long patientId;

}
