package com.example.iqviaassignment.constant;

public class UrlConstant {
	public static final String URL_PatientByEmail = "/patientByEmail";
	public static final String URL_PatientByName = "/patientByName";
	public static final String URL_Patient = "/patient";
	public static final String URL_UpdatePatient = "/UpdatePatient";
	public static final String URL_medication = "/medicationData";
	public static final String URL_Medications = "/medication";
	
}
